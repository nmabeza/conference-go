from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_AccountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    #   updated = convert updated_string from ISO string to datetime

    if is_active:
        #       Use the update_or_create method of the AccountVO.objects QuerySet
        #           to update or create the AccountVO object
        AccountVO.objects.update_or_create(
            {
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "is_active": is_active,
                "updated": updated,
            }
        )
    else:
        account = AccountVO.objects.filter(email=email)
        AccountVO.objects.delete(account)


#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
#   try

while True:
    try:

        def main():
            #       create the pika connection parameters
            parameters = pika.ConnectionParameters(host="rabbitmq")
            #       create a blocking connection with the parameters
            connection = pika.BlockingConnection(parameters)
            #       open a channel
            channel = connection.channel()
            #       declare a fanout exchange named "account_info"
            channel.exchange_declare(
                exchange="account_info", exchange_type="fanout"
            )
            #       declare a randomly-named queue
            result = channel.queue_declare(queue="", exclusive=True)
            queue_name = result.method.queue
            #       get the queue name of the randomly-named queue
            channel.queue_bind(exchange="account_info", queue=queue_name)
            #       bind the queue to the "account_info" exchange
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=update_AccountVO,
                auto_ack=True,
            )
            #       do a basic_consume for the queue name that calls
            #           function above
            channel.start_consuming()

        #       tell the channel to start consuming

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        #       print that it could not connect to RabbitMQ
        time.sleep(2.0)
#       have it sleep for a couple of seconds
